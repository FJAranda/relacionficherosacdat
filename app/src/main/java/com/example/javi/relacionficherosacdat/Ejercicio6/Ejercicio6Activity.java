package com.example.javi.relacionficherosacdat.Ejercicio6;

import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.example.javi.relacionficherosacdat.Ejercicio4.RestClient;
import com.example.javi.relacionficherosacdat.R;
import com.loopj.android.http.FileAsyncHttpResponseHandler;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

import cz.msebera.android.httpclient.Header;

public class Ejercicio6Activity extends AppCompatActivity {

    private Button btnConvertir;
    private RadioButton rbTipoConversion;
    private EditText etEuro;
    private EditText etDolar;
    private String cambio;
    private ConstraintLayout clEjercicio6;

    final String urlCambio = "http://alumno.mobi/~alumno/superior/aranda/cambio.txt";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio6);
        etDolar = (EditText) findViewById(R.id.etDolares);
        etEuro = (EditText) findViewById(R.id.etEuros);
        clEjercicio6 = (ConstraintLayout)findViewById(R.id.clEjercicio6);
        btnConvertir = (Button)findViewById(R.id.btnConvertir);
        btnConvertir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                leerCambio();
                rbTipoConversion=(RadioButton) findViewById(R.id.rbEurosADolares);
                if (rbTipoConversion.isChecked())
                {
                    try {
                        etDolar.setText(convertirADolares(etEuro.getText().toString(),cambio));
                    }catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }else
                {
                    try {
                        etEuro.setText(convertirAEuros(String.format(etDolar.getText().toString()),cambio));
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    public String convertirADolares(String cantidad,String cambio) {
        double valor =
                Double.parseDouble(cantidad) / Double.parseDouble(cambio);
        return
                Double.toString(valor);
    }

    public String convertirAEuros(String cantidad,String cambio) {
        double valor =
                Double.parseDouble(cantidad) * Double.parseDouble(cambio);
        return
                Double.toString(valor);
    }

    public void leerCambio(){
        File Fichero = new File (Environment.getExternalStorageDirectory().getAbsolutePath());
        RestClient.get(urlCambio, new FileAsyncHttpResponseHandler(Fichero) {
            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                Snackbar.make(clEjercicio6, "Error " + statusCode + " : " + throwable.getMessage(), Snackbar.LENGTH_LONG).show();
            }
            @Override
            public void onSuccess(int statusCode, Header[] headers, File file) {
                String cadena=null;
                FileInputStream fis = null;
                try {
                    fis = new FileInputStream(file);
                    BufferedReader in = new BufferedReader(new InputStreamReader(fis));
                    while ((cadena = in.readLine()) != null) {
                        cambio=cadena;
                    }
                    in.close();
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
