package com.example.javi.relacionficherosacdat.Ejercicio1;

import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.javi.relacionficherosacdat.R;

import java.util.List;

/**
 * Created by Javi on 13/11/17.
 */

public class rvAgendaAdapter extends RecyclerView.Adapter<rvAgendaAdapter.ContactosViewHolder> {

    List<Contacto> agenda;

    rvAgendaAdapter(List<Contacto> agenda){
        this.agenda = agenda;
    }

    public static class ContactosViewHolder extends RecyclerView.ViewHolder{

        ConstraintLayout clContactoAgenda;
        TextView nombreContacto;
        TextView telefonoContacto;
        TextView mailContacto;

        public ContactosViewHolder(View itemView) {
            super(itemView);
            clContactoAgenda = (ConstraintLayout)itemView.findViewById(R.id.clContactos);
            nombreContacto = (TextView)itemView.findViewById(R.id.tvNombreContacto);
            telefonoContacto = (TextView)itemView.findViewById(R.id.tvTelefonoContacto);
            mailContacto = (TextView)itemView.findViewById(R.id.tvMailContacto);
        }
    }

    @Override
    public rvAgendaAdapter.ContactosViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.contacto_agenda, parent, false);
        ContactosViewHolder viewHolder = new ContactosViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(rvAgendaAdapter.ContactosViewHolder holder, int position) {
        holder.nombreContacto.setText(agenda.get(position).getNombre());
        holder.telefonoContacto.setText(agenda.get(position).getTelefono());
        holder.mailContacto.setText(agenda.get(position).getMail());
    }

    @Override
    public int getItemCount() {
        return agenda.size();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
