package com.example.javi.relacionficherosacdat.Ejercicio2;

import java.io.Serializable;

/**
 * Created by el_ja on 27/11/2017.
 */

public class Alarma{
    private int minutos;
    private String mensaje;

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public Alarma(int minutos, String mensaje) {
        this.minutos = minutos;
        this.mensaje = mensaje;
    }

    @Override
    public String toString() {
        return minutos + "," + mensaje;
    }
}
