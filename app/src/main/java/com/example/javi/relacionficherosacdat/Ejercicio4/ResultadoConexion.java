package com.example.javi.relacionficherosacdat.Ejercicio4;

/**
 * Created by el_ja on 28/11/2017.
 */

public class ResultadoConexion {
    private boolean resultado;
    private String mensaje;
    private String contenido;
    public boolean getResultado() {
        return resultado;
    }
    public void setResultado(boolean resultado) {
        this.resultado = resultado;
    }
    public String getMensaje() {
        return mensaje;
    }
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }
    public String getContenido() {
        return contenido;
    }
    public void setContenido(String contenido) {
        this.contenido = contenido;
    }
}
