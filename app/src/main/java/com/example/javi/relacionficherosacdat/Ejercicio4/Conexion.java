package com.example.javi.relacionficherosacdat.Ejercicio4;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import cz.msebera.android.httpclient.HttpResponse;
import cz.msebera.android.httpclient.client.methods.HttpPost;
import cz.msebera.android.httpclient.impl.client.CloseableHttpClient;
import cz.msebera.android.httpclient.impl.client.HttpClientBuilder;

/**
 * Created by el_ja on 28/11/2017.
 */

public class Conexion {
    private static String leer(InputStream entrada) throws IOException {
        BufferedReader in;
        String linea;
        StringBuilder miCadena = new StringBuilder();
        in = new BufferedReader(new InputStreamReader(entrada), 32000);
        while ((linea = in.readLine()) != null)
            miCadena.append(linea);
//miCadena.append(linea).append('\n');
        in.close();
        return miCadena.toString();
    }

    public static ResultadoConexion conectarJava(String texto) {
        URL url;
        HttpURLConnection urlConnection = null;
        int respuesta;
        ResultadoConexion resultado = new ResultadoConexion();
        try {
            url = new URL(texto);
            urlConnection = (HttpURLConnection) url.openConnection();
            respuesta = urlConnection.getResponseCode();
            if (respuesta == HttpURLConnection.HTTP_OK) {
                resultado.setResultado(true);
                resultado.setContenido(leer(urlConnection.getInputStream()));
            } else {
                resultado.setResultado(false);
                resultado.setMensaje("Error en el acceso a la web: " + String.valueOf(respuesta));
            }
        } catch (IOException e) {
            resultado.setResultado(false);
            resultado.setMensaje("Excepción: " + e.getMessage());
        } finally {
            try {
                if (urlConnection != null)
                    urlConnection.disconnect();
            } catch (Exception e) {
                resultado.setResultado(false);
                resultado.setMensaje("Excepción: " + e.getMessage());
            }
            return resultado;
        }
    }

    public static ResultadoConexion conectarApache(String texto) {
        CloseableHttpClient cliente = null;
        HttpPost peticion;
        HttpResponse respuesta;
        int valor;
        ResultadoConexion resultado = new ResultadoConexion();
        try {
            //cliente = new DefaultHttpClient();
            cliente = HttpClientBuilder.create().build();
            peticion = new HttpPost(texto);
            respuesta = cliente.execute(peticion);
            valor = respuesta.getStatusLine().getStatusCode();
            if (valor == HttpURLConnection.HTTP_OK) {
                resultado.setResultado(true);
                resultado.setContenido(leer(respuesta.getEntity().getContent()));
            } else {
                resultado.setResultado(false);
                resultado.setMensaje("Error en el acceso a la web: " + String.valueOf(valor));
            }
            cliente.close();
        } catch (IOException e) {
            resultado.setResultado(false);
            resultado.setMensaje("Excepción: " + e.getMessage());
            if (cliente != null)
                try {
                    cliente.close();
                } catch (IOException excep) {
                    resultado.setResultado(false);
                    resultado.setMensaje("Excepción: " + excep.getMessage());
                }
        }
        return resultado;
    }
}
