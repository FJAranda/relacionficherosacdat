package com.example.javi.relacionficherosacdat.Ejercicio7;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.javi.relacionficherosacdat.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.File;
import java.io.FileNotFoundException;

public class Ejercicio7Activity extends AppCompatActivity implements View.OnClickListener {

    private TextView tvRuta;
    private Button btnSubir;
    private Button btnExplorar;
    private File file;
    private ConstraintLayout clEjercicio7;
    private static final int CODIGO_EXPLORAR = 0;
    private static final String SERVIDOR = "http://alumno.mobi/~alumno/superior/aranda/";
    private static final int MAX_TIMEOUT = 2000;
    private static final int RETRIES = 1;
    private static final int TIMEOUT_BETWEEN_RETRIES = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio7);
        tvRuta = (TextView) findViewById(R.id.tvRutaArchivo);
        clEjercicio7 = (ConstraintLayout)findViewById(R.id.clEjercicio7);
        btnSubir = (Button) findViewById(R.id.btnSubir);
        btnSubir.setOnClickListener(this);
        btnExplorar = (Button) findViewById(R.id.btnExplorar);
        btnExplorar.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSubir:
                subir();
                break;
            case R.id.btnExplorar:
                explorar();
                break;

        }
    }
    private void subir() {
        final ProgressDialog progreso = new ProgressDialog(Ejercicio7Activity.this);
        final AsyncHttpClient cliente = new AsyncHttpClient();
        RequestParams params = new RequestParams();
        cliente.setTimeout(MAX_TIMEOUT);
        cliente.setMaxRetriesAndTimeout(RETRIES, TIMEOUT_BETWEEN_RETRIES);
        if (!tvRuta.getText().toString().trim().isEmpty()) {
            file = new File(tvRuta.getText().toString());
            if (file.length() < 100000) {
                try {
                    params.put("fileToUpload", file);

                    cliente.post(SERVIDOR, params, new TextHttpResponseHandler() {

                        @Override
                        public void onFailure(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString, Throwable throwable) {
                            progreso.dismiss();
                            Snackbar.make(clEjercicio7, "Error al subir el archivo: " + statusCode, Snackbar.LENGTH_LONG).show();
                        }

                        @Override
                        public void onSuccess(int statusCode, cz.msebera.android.httpclient.Header[] headers, String responseString) {
                            progreso.dismiss();
                            Snackbar.make(clEjercicio7, "Exito al subir!", Snackbar.LENGTH_LONG).show();
                        }

                        @Override
                        public void onStart() {
                            super.onStart();
                            progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                            progreso.setMessage("Subiendo...");
                            progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {
                                    cliente.cancelAllRequests(true);
                                }
                            });
                            progreso.show();
                        }
                    });
                } catch (FileNotFoundException e) {
                    Snackbar.make(clEjercicio7, "El archivo a subir no existe...", Snackbar.LENGTH_LONG).show();
                }
            } else {
                Snackbar.make(clEjercicio7, "El archivo es demasiado grande...", Snackbar.LENGTH_LONG).show();
            }
        }
    }

    private void explorar() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("file/*");
        String[] extraMIMETypes = {"image/jpeg", "image/png", "text/html", "text/plain", "audio/mpeg3", "application/pdf"};
        intent.putExtra(Intent.EXTRA_MIME_TYPES, extraMIMETypes);
        if (intent.resolveActivity(getPackageManager()) != null)
            startActivityForResult(intent, CODIGO_EXPLORAR);
        else {
            Snackbar.make(clEjercicio7, "Descarga una aplicacion para explorar...", Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 0) {
            if (resultCode == RESULT_OK) {
                tvRuta.setText(data.getData().getPath());
            }
        }
    }
}
