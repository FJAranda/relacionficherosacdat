package com.example.javi.relacionficherosacdat.Ejercicio3;

import android.content.Context;
import android.os.Environment;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.javi.relacionficherosacdat.Ejercicio2.Alarma;
import com.example.javi.relacionficherosacdat.R;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Ejercicio3Activity extends AppCompatActivity {

    List<String> fechas = new ArrayList<>();
    private static final String FICHERO = "calendarioescolar.txt";
    private ConstraintLayout clEjercicio3;
    private TextView tvFechaActual;
    private TextView tvDiasLectivos;
    private Button btnCalcularLectivos;
    private EditText etFechaInicial;
    private EditText etFechaFinal;
    private DateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio3);
        clEjercicio3 = (ConstraintLayout)findViewById(R.id.clEjercicio3);
        tvDiasLectivos = (TextView)findViewById(R.id.tvDiasLectivos);
        tvFechaActual = (TextView)findViewById(R.id.tvFechaActual);
        etFechaFinal = (EditText)findViewById(R.id.etFechaFinal);
        etFechaInicial = (EditText)findViewById(R.id.etFechaIncial);
        btnCalcularLectivos = (Button)findViewById(R.id.btnCalcularDiasLectivos);
        btnCalcularLectivos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Date fechaInicio;
                Date fechaFin;
                try{
                    fechaInicio = formatter.parse(etFechaInicial.getText().toString());
                    fechaFin = formatter.parse(etFechaFinal.getText().toString());

                    tvDiasLectivos.setText("Entre " + etFechaInicial.getText().toString() + " y " + etFechaFinal.getText().toString() + " hay " + contarDias(fechaInicio, fechaFin)
                    +" dia/s lectivos. Fechas no incluidas.");

                } catch (ParseException e) {
                    e.printStackTrace();
                    Snackbar.make(clEjercicio3, "Imposible convertir una de las fechas", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        crearArrayFechas();

        Calendar calendar = Calendar.getInstance();
        String fecha = Integer.toString(calendar.get(Calendar.DATE)) + "-" +Integer.toString(calendar.get(Calendar.MONTH)+1) + "-" +Integer.toString(calendar.get(Calendar.YEAR));

        if(comprobarLectivo(fecha)){
            tvFechaActual.setText( fecha + ": Hoy SI es un dia lectivo");
        }else{
            tvFechaActual.setText(fecha + ": Hoy NO es un dia lectivo");
        }
    }

    private int contarDias(Date fechaInicio, Date fechaFin) {
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        int dias = 0;
        Date fechaLectura;
        String linea = "";

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            try {
                File ruta = Environment.getExternalStorageDirectory();
                File f = new File(ruta.getAbsolutePath(), FICHERO);
                fileInputStream = new FileInputStream(f);
                inputStreamReader = new InputStreamReader(fileInputStream);
                bufferedReader = new BufferedReader(inputStreamReader);

                while (((linea = bufferedReader.readLine()) != null)) {
                    fechaLectura = formatter.parse(linea);
                    if (fechaLectura.after(fechaInicio) && fechaLectura.before(fechaFin) )
                    {
                        dias+=1;
                    }
                }

                bufferedReader.close();
                inputStreamReader.close();
                fileInputStream.close();
            } catch (Exception e) {
                Log.d("Error:", "Error al leer: " + e.getMessage());
            }
        }
        return dias;
    }

    private boolean comprobarLectivo(String fecha) {
        FileInputStream fileInputStream = null;
        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        boolean lectivo = false;
        String linea = "";

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            try {
                File ruta = Environment.getExternalStorageDirectory();
                File f = new File(ruta.getAbsolutePath(), FICHERO);
                fileInputStream = new FileInputStream(f);
                inputStreamReader = new InputStreamReader(fileInputStream);
                bufferedReader = new BufferedReader(inputStreamReader);

                while (((linea = bufferedReader.readLine()) != null) && lectivo == false) {
                    if(linea.equals(fecha)){
                        lectivo = true;
                    }
                }

                bufferedReader.close();
                inputStreamReader.close();
                fileInputStream.close();
            } catch (Exception e) {
                Log.d("Error:", "Error al leer: " + e.getMessage());
            }
        }
        return lectivo;
    }

    private void crearArrayFechas() {
        String anio = "2017";
        String fecha = "";
        int dia = 5;
        for (int i = 0; i<10; i++ ){
            for (int j = 1; j < 32; j++){
                if(i == 0) {
                    if (j > 14 && j < 31) {
                        if (dia < 6) {
                            fecha = String.valueOf(j) + "-09-" + anio;
                            fechas.add(fecha);
                        }else{
                            if (dia >= 7) {
                                dia = 0;
                            }
                        }
                        dia += 1;
                    }
                }else
                if(i == 1 ) {
                    if (dia < 6 && j != 12) {
                        fecha = String.valueOf(j) + "-10-" + anio;
                        fechas.add(fecha);
                    }else{
                        if (dia >= 7) {
                            dia = 0;
                        }
                    }
                    dia += 1;
                }else
                if(i == 2) {
                    if (j != 31) {
                        if (dia < 6 && j!=1) {
                            fecha = String.valueOf(j) + "-11-" + anio;
                            fechas.add(fecha);
                        }else{
                            if (dia >= 7) {
                                dia = 0;
                            }
                        }
                        dia += 1;
                    }
                }else
                if(i == 3) {
                    if (dia < 6 && j < 23 && j!= 6 && j != 8) {
                        fecha = String.valueOf(j) + "-12-" + anio;
                        fechas.add(fecha);
                    }else{
                        if (dia >= 7) {
                            dia = 0;
                        }
                    }
                    dia += 1;
                }else
                if(i == 4) {
                    if (dia < 6 && j > 7 ) {
                        fecha = String.valueOf(j) + "-01-" + anio;
                        fechas.add(fecha);
                    }else{
                        if (dia >= 7) {
                            dia = 0;
                        }
                    }
                    dia += 1;
                }
                if(i == 5) {
                    if (j < 29) {
                        if (dia < 6 && j<24) {
                            fecha = String.valueOf(j) + "-02-" + anio;
                            fechas.add(fecha);
                        }else{
                            if (dia >= 7) {
                                dia = 0;
                            }
                        }
                        dia += 1;
                    }
                }else
                if(i == 6) {
                    if (dia < 6 && j >4 && j < 24) {
                        fecha = String.valueOf(j) + "-03-" + anio;
                        fechas.add(fecha);
                    }else{
                        if (dia >= 7) {
                            dia = 0;
                        }
                    }
                    dia += 1;
                }else
                if(i == 7) {
                    if (j != 31) {
                        if (dia < 6) {
                            fecha = String.valueOf(j) + "-04-" + anio;
                            fechas.add(fecha);
                        }else{
                            if (dia >= 7) {
                                dia = 0;
                            }
                        }
                        dia += 1;
                    }
                }else
                if(i == 8) {
                    if (dia < 6 && j != 1) {
                        fecha = String.valueOf(j) + "-05-" + anio;
                        fechas.add(fecha);
                    }else{
                        if (dia >= 7) {
                            dia = 0;
                        }
                    }
                    dia += 1;
                }else
                if(i == 9) {
                    if (j != 31 && j < 23){
                        if (dia < 6) {
                            fecha = String.valueOf(j) + "-06-" + anio;
                            fechas.add(fecha);
                        }else{
                            if (dia >= 7) {
                                dia = 0;
                            }
                        }
                        dia += 1;
                    }
                }
            }
        }
        if (guardarArrayFechas()){
            Snackbar.make(clEjercicio3, "Fichero de fechas creado en memoria externa!", Snackbar.LENGTH_LONG).show();
        }else{
            Snackbar.make(clEjercicio3, "Hubo algun error...", Snackbar.LENGTH_LONG).show();
        }
    }

    private boolean guardarArrayFechas(){
        FileOutputStream fileOutputStream = null;
        OutputStreamWriter outputStreamWriter = null;
        BufferedWriter bufferedWriter = null;

        if (Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
            try{
                File ruta = Environment.getExternalStorageDirectory();
                File f = new File(ruta.getAbsolutePath(), FICHERO);
                if ((f.exists())){
                    f.delete();
                }
                f.createNewFile();
                fileOutputStream = new FileOutputStream(f);
                outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                bufferedWriter = new BufferedWriter(outputStreamWriter);
                for (int i = 0; i < fechas.size(); i++) {
                    bufferedWriter.write(fechas.get(i).toString());
                    bufferedWriter.newLine();
                }
                bufferedWriter.close();
                outputStreamWriter.close();
                fileOutputStream.close();
                return true;

            } catch (FileNotFoundException e) {
                e.printStackTrace();
                return false;
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }else{
            return false;
        }
    }
}
