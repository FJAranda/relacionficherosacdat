package com.example.javi.relacionficherosacdat.Ejercicio4;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.StrictMode;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.javi.relacionficherosacdat.R;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.UnsupportedEncodingException;

import cz.msebera.android.httpclient.Header;

public class Ejercicio4Activity extends AppCompatActivity implements View.OnClickListener {

    private EditText etURL ;
    private WebView wvWebview;
    private RadioButton rbJava;
    private RadioButton rbAAHC;
    private RadioButton rbVolley;
    private ResultadoConexion resultado = null;
    private Button btnConectar;
    private Button btnDescargar;
    private ConstraintLayout clEjercicio4;

    RequestQueue mRequestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio4);
        etURL = (EditText) findViewById(R.id.etURL);
        wvWebview = (WebView) findViewById(R.id.wvEjercicio4);
        rbJava = (RadioButton) findViewById(R.id.rbJava);
        rbAAHC = (RadioButton) findViewById(R.id.rbAAHC);
        rbVolley = (RadioButton) findViewById(R.id.rbVolley);
        btnConectar = (Button)findViewById(R.id.btnConectar);
        btnConectar.setOnClickListener(this);
        clEjercicio4 = (ConstraintLayout)findViewById(R.id.clEjercicio4);

        StrictMode.setThreadPolicy(new StrictMode.ThreadPolicy.Builder().permitNetwork().build());
    }

    @Override
    public void onClick(View view) {
        if(view==btnConectar)
            establecerConexion();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        return networkInfo != null && networkInfo.isConnected();
    }

    private void establecerConexion() {
        if (isNetworkAvailable()) {
            if (rbJava.isChecked())
                conexionJava();
            if (rbAAHC.isChecked())
                conexionAAHC();
            if (rbVolley.isChecked())
                conexionVolley();
        }
        else
        {
            Snackbar.make(clEjercicio4,"Sin conexion a la red", Snackbar.LENGTH_LONG).show();
        }
    }

    private void conexionJava()
    {
        resultado = Conexion.conectarJava(etURL.getText().toString());
        if (resultado.getResultado())
            wvWebview.loadDataWithBaseURL(null, resultado.getContenido(), "text/html", "UTF-8", null);
        else
            wvWebview.loadDataWithBaseURL(null, resultado.getMensaje(), "text/html", "UTF-8", null);
    }

    private void conexionAAHC() {
        final String texto = etURL.getText().toString();
        final ProgressDialog progreso = new ProgressDialog(Ejercicio4Activity.this);
        RestClient.get(texto, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                //called before request is started
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando...");
                //progreso.setCancelable(false);
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                // called when response HTTP status is "200 OK"
                progreso.dismiss();
                wvWebview.loadDataWithBaseURL(null, response, "text/html", "UTF-8", null);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                // called when response HTTP status is "4XX" (eg. 401, 403, 404)
                progreso.dismiss();
                wvWebview.loadDataWithBaseURL(null, "Error: " + response + " ", "text/html", "UTF-8", null);
            }
        });
    }

    public void conexionVolley() {
        final String enlace = etURL.getText().toString();
        // Instantiate the RequestQueue.
        mRequestQueue = Volley.newRequestQueue(this);

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Conectando...");
        progressDialog.setCancelable(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                mRequestQueue.cancelAll("tag");
            }
        });

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, enlace, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                wvWebview.loadDataWithBaseURL(enlace, response, "text/html", "UTF-8", null);

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String mensaje = "Error";
                if (error instanceof TimeoutError || error instanceof NoConnectionError)
                    mensaje = "Timeout Error: " + error.getMessage();
                else {
                    NetworkResponse errorResponse = error.networkResponse;
                    if (errorResponse != null && errorResponse.data != null) {
                        try {
                            mensaje = "Error: " + errorResponse.statusCode + " " + "\n " +
                                    new String(errorResponse.data, "UTF-8");

                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }
                }
                progressDialog.dismiss();
                wvWebview.loadDataWithBaseURL(null, mensaje, "text/html", "UTF-8", null);
            }
        });
        // Set the tag on the request.
        stringRequest.setTag("tag");
        // Set retry policy
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(3000, 1, 1));
        // Add the request to the RequestQueue.
        mRequestQueue.add(stringRequest);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mRequestQueue != null) {
            mRequestQueue.cancelAll("tag");
        }
    }
}
