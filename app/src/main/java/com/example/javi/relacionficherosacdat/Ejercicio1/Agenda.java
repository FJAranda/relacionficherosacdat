package com.example.javi.relacionficherosacdat.Ejercicio1;

import android.content.Context;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javi on 23/11/17.
 */

public class Agenda {
    private static final String FICHERO = "agenda.dat";

    public static List<Contacto> getAgenda() {
        return agenda;
    }

    private static List<Contacto> agenda = new ArrayList<>();

    public static boolean addContacto(Contacto contacto){
        return agenda.add(contacto);
    }

    public static String WriteAgendaFileInternal(Context context){

        FileOutputStream fileOutputStream = null;
        ObjectOutputStream objectOutputStream = null;
        String resultado = "";

        try {
            fileOutputStream = context.openFileOutput(FICHERO, Context.MODE_PRIVATE);
            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            for (int i = 0; i < agenda.size(); i++) {
                objectOutputStream.writeObject(agenda.get(i));
            }
            objectOutputStream.close();
            fileOutputStream.close();
        }catch (IOException e){
            resultado = "Error al escribir: " + e.getMessage();
        }
        resultado = "Exito al escribir!";
        return  resultado;
    }

    public static String ReadAgendaFileInternal(Context context){

        FileInputStream fileInputStream = null;
        ObjectInputStream objectInputStream = null;
        Contacto contacto = null;
        String resultado = "";

        try {
            fileInputStream = context.openFileInput(FICHERO);
            objectInputStream = new ObjectInputStream(fileInputStream);
            while((contacto = (Contacto)objectInputStream.readObject()) != null){
                agenda.add(contacto);
            }
            objectInputStream.close();
            fileInputStream.close();
        }catch (Exception e){
            resultado = "Error al leer: " + e.getMessage();
        }
        resultado = "Exito al leer!";
        return resultado;
    }
}
