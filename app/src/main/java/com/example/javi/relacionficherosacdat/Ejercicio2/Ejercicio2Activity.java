package com.example.javi.relacionficherosacdat.Ejercicio2;

import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.CountDownTimer;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.javi.relacionficherosacdat.R;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Ejercicio2Activity extends AppCompatActivity {

    private NumberPicker npMinutos;
    private EditText etMensaje;
    private ListView lvAlarmas;
    private List<Alarma> alarmas;
    private Button btnAnadirAlarma;
    private Button btnEmpezar;
    private TextView tvTiempoRestante;
    private TextView tvAlarmasRestantes;
    private AlarmasAdapter adapter;
    private ConstraintLayout clEjercicio2;
    private MediaPlayer mp;
    private static int totalAlarmas = 0;
    private static int alarmasInicial = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio2);
        npMinutos = (NumberPicker)findViewById(R.id.numberPicker);
        etMensaje = (EditText)findViewById(R.id.editText3);
        lvAlarmas = (ListView)findViewById(R.id.lvAlarmas);
        alarmas = AlarmaUtility.readAlarmas(this);
        btnAnadirAlarma = (Button)findViewById(R.id.button8);
        btnEmpezar = (Button)findViewById(R.id.button9);
        adapter = new AlarmasAdapter(this, alarmas);
        tvTiempoRestante = (TextView) findViewById(R.id.tvTiempoSiguienteAlarma);
        tvAlarmasRestantes = (TextView) findViewById(R.id.tvAlarmasRestantes);
        clEjercicio2 = (ConstraintLayout)findViewById(R.id.clEjercicio2);

        npMinutos.setMinValue(1);
        npMinutos.setMaxValue(60);
        npMinutos.setWrapSelectorWheel(true);

        lvAlarmas.setAdapter(adapter);

        btnAnadirAlarma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Alarma alarma = new Alarma(npMinutos.getValue(), etMensaje.getText().toString());
                AlarmaUtility.writeAlarma(alarma, getBaseContext());
                adapter.updateList(AlarmaUtility.readAlarmas(getBaseContext()));
            }
        });

        btnEmpezar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alarmasInicial = adapter.getCount();
                totalAlarmas = adapter.getCount();
                siguienteAlarma();
                Snackbar.make(clEjercicio2, "Alarmas Comenzadas!", Snackbar.LENGTH_LONG).show();
            }
        });
    }

    public class MyCountDownTimer extends CountDownTimer {
        private String mensaje;
        public MyCountDownTimer(long startTime, long interval, String mensaje) {
            super(startTime, interval);
            this.mensaje = mensaje;
        }

        @Override
        public void onTick(long millisUntilFinished) {
            long minutos = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished);
            long segundos = TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - (minutos*60);
            tvTiempoRestante.setText(String.format("%02d", minutos) + ":" + (String.format("%02d", segundos)));
        }
        @Override
        public void onFinish() {
            Snackbar.make(clEjercicio2, mensaje, Snackbar.LENGTH_LONG).show();
            mp = MediaPlayer.create(Ejercicio2Activity.this, R.raw.alien);
            mp.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    mp.start();
                }
            });

            mp.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mp.release();
                }
            });

            totalAlarmas -= 1;
            tvAlarmasRestantes.setText(String.valueOf(totalAlarmas));
            siguienteAlarma();
        }
    }

    private void siguienteAlarma() {
        if (totalAlarmas > 0) {
            Alarma alarma = alarmas.get(alarmasInicial - totalAlarmas);
            new MyCountDownTimer(alarma.getMinutos() * 1000, 1000, alarma.getMensaje()).start();
        }
    }
}
