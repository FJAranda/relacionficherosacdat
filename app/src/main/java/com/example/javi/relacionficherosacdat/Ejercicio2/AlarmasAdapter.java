package com.example.javi.relacionficherosacdat.Ejercicio2;

import android.content.ClipData;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.javi.relacionficherosacdat.R;

import java.util.List;

/**
 * Created by Javi on 27/11/17.
 */

public class AlarmasAdapter extends BaseAdapter {
    private List<Alarma> alarmas;
    private Context context;

    public AlarmasAdapter(Context context, List<Alarma> alarmas){
        this.context = context;
        this.alarmas = alarmas;
    }

    public void updateList(List<Alarma> alarmas){
        this.alarmas.clear();
        this.alarmas.addAll(AlarmaUtility.readAlarmas(context));
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return alarmas.size();
    }

    @Override
    public Object getItem(int i) {
        return this.alarmas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View rowView = view;

        if (view == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            rowView = inflater.inflate(R.layout.list_alarma_item, viewGroup, false);
        }

        TextView tvTiempo = (TextView)rowView.findViewById(R.id.tvTiempo);
        TextView tvMensaje = (TextView)rowView.findViewById(R.id.tvMensajeAlarma);

        Alarma alarma = this.alarmas.get(i);
        tvTiempo.setText(String.valueOf(alarma.getMinutos()));
        tvMensaje.setText(alarma.getMensaje());

        return rowView;
    }
}
