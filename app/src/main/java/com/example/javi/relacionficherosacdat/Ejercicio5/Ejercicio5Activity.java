package com.example.javi.relacionficherosacdat.Ejercicio5;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.example.javi.relacionficherosacdat.R;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.FileAsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import cz.msebera.android.httpclient.Header;

public class Ejercicio5Activity extends AppCompatActivity implements View.OnClickListener {

    private EditText etURL;
    private Button btnDescargar;
    private Button btnSiguiente;
    private Button btnAnterior;
    private ImageView ivImagen;
    private ConstraintLayout clEjercicio5;

    private ArrayList<String> urls;
    private boolean resultado;

    private int imagenActual;

    private static final int MAX_TIMEOUT = 2000;
    private static final int RETRIES = 1;
    private static final int TIMEOUT_BETWEEN_RETRIES = 5000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio5);

        etURL = (EditText) findViewById(R.id.etURLEjercicio5);
        btnDescargar = (Button) findViewById(R.id.btnDescargar);
        btnDescargar.setOnClickListener(this);
        btnSiguiente = (Button) findViewById(R.id.btnSiguiente);
        btnSiguiente.setOnClickListener(this);
        btnAnterior = (Button) findViewById(R.id.btnAnterior);
        btnAnterior.setOnClickListener(this);
        ivImagen = (ImageView) findViewById(R.id.ivImagen);
        clEjercicio5 = (ConstraintLayout)findViewById(R.id.clEjercicio5);

        urls = new ArrayList<>();
        resultado = false;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnDescargar:
                descargarFichero();
                if (resultado) {
                    ponerPrimeraImagen();
                    imagenActual = 0;
                }
                break;
            case R.id.btnSiguiente:
                if (resultado) {
                    imagenActual++;
                    Picasso.with(this)
                            .load(urls.get(imagenActual % (urls.size() - 1)))
                            .into(ivImagen);
                }
                break;
            case R.id.btnAnterior:
                if (resultado) {
                    if (imagenActual > 0) {
                        imagenActual--;
                    } else {
                        imagenActual = urls.size() - 1;
                    }
                    Picasso.with(this)
                            .load(urls.get(imagenActual % (urls.size() - 1)))
                            .into(ivImagen);
                }
                break;
        }
    }

    private void descargarFichero() {
        final ProgressDialog progreso = new ProgressDialog(this);
        final AsyncHttpClient cliente = new AsyncHttpClient();
        cliente.setTimeout(MAX_TIMEOUT);
        cliente.setMaxRetriesAndTimeout(RETRIES, TIMEOUT_BETWEEN_RETRIES);

        if (URLUtil.isValidUrl(etURL.getText().toString())) {
            cliente.get(etURL.getText().toString(), new FileAsyncHttpResponseHandler(this) {

                @Override
                public void onStart() {
                    progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progreso.setMessage("Descargando fichero...");
                    progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialogInterface) {
                            cliente.cancelAllRequests(true);
                        }
                    });
                    progreso.show();
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, File file) {
                    progreso.dismiss();
                    Snackbar.make(clEjercicio5, "Error al descargar el archivo de enlaces...", Snackbar.LENGTH_LONG).show();
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, File file) {
                    String linea;
                    try {
                        FileInputStream fis = new FileInputStream(file);
                        InputStreamReader isr = new InputStreamReader(fis);
                        BufferedReader br = new BufferedReader(isr);
                        while ((linea = br.readLine()) != null) {
                            urls.add(linea);
                        }
                    } catch (FileNotFoundException e) {
                        Snackbar.make(clEjercicio5, "El archivo descargado parece no existir...", Snackbar.LENGTH_LONG).show();
                    } catch (IOException e) {
                        Snackbar.make(clEjercicio5, "Error al leer el archivo...", Snackbar.LENGTH_LONG).show();
                    }
                    resultado = true;
                    progreso.dismiss();
                }
            });

        } else {
            Snackbar.make(clEjercicio5, "Parece que la direccion del fichero esta mal...", Snackbar.LENGTH_LONG).show();
        }
    }
    private void ponerPrimeraImagen() {
        if (!urls.isEmpty()) {
            Picasso.with(Ejercicio5Activity.this).load(urls.get(0)).into(ivImagen);
        } else {
            Snackbar.make(clEjercicio5, "No se han conseguido direcciones del fichero...", Snackbar.LENGTH_LONG).show();
        }
    }
}
