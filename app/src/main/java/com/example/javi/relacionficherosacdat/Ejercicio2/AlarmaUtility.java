package com.example.javi.relacionficherosacdat.Ejercicio2;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.util.Log;

import com.example.javi.relacionficherosacdat.Ejercicio1.Contacto;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.File;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by el_ja on 27/11/2017.
 */

public class AlarmaUtility {
    private static final String FICHERO = "alarma.txt";
    private static List<Alarma> alarmas = new ArrayList<>();


    public static List<Alarma> getAlarmas() {
        return alarmas;
    }

    public static List<Alarma> readAlarmas(Context context){

        InputStreamReader inputStreamReader = null;
        BufferedReader bufferedReader = null;
        Alarma alarma = null;
        List<Alarma> alarmas = new ArrayList<>();
        String linea = "";

        try {
            inputStreamReader = new InputStreamReader(context.openFileInput(FICHERO));
            bufferedReader = new BufferedReader(inputStreamReader);

            while((linea = bufferedReader.readLine()) != ""){
                alarma = new Alarma(Integer.parseInt((linea.split(","))[0]), (linea.split(","))[1]);
                alarmas.add(alarma);
            }

            bufferedReader.close();
            inputStreamReader.close();
        }catch (Exception e){
            Log.d("Error:","Error al leer: " + e.getMessage());
        }
        return alarmas;
    }

    public static boolean writeAlarma(Alarma alarma, Context context) {
        List<Alarma> alarmas = readAlarmas(context);
        if (alarmas.size() < 5) {
            alarmas.add(alarma);
            FileOutputStream fileOutputStream = null;
            OutputStreamWriter outputStreamWriter = null;
            BufferedWriter bufferedWriter = null;
            try {
                fileOutputStream = context.openFileOutput(FICHERO, Context.MODE_PRIVATE);
                outputStreamWriter = new OutputStreamWriter(fileOutputStream);
                bufferedWriter = new BufferedWriter(outputStreamWriter);
                for (int i = 0; i < alarmas.size(); i++) {
                    bufferedWriter.write(alarmas.get(i).toString());
                    bufferedWriter.newLine();
                }
                bufferedWriter.close();
                outputStreamWriter.close();
                fileOutputStream.close();
            } catch (IOException e) {
                Log.d("AlarmaUtility", "Error al intentar escribir las alarmas");
            }
            return true;
        } else {
            return false;
        }
    }
}
