package com.example.javi.relacionficherosacdat.Ejercicio1;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.support.design.widget.Snackbar;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Javi on 13/11/17.
 */

public class Contacto implements Serializable{

    private String nombre;
    private String telefono;
    private String mail;

    public String getNombre() {
        return nombre;
    }
    public String getTelefono() {
        return telefono;
    }
    public String getMail() {
        return mail;
    }

    public Contacto(String nombre, String telefono, String mail){
        this.nombre = nombre;
        this.telefono = telefono;
        this.mail = mail;
    }
}
