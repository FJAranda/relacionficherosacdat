package com.example.javi.relacionficherosacdat;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.example.javi.relacionficherosacdat.Ejercicio1.Ejercicio1Activity;
import com.example.javi.relacionficherosacdat.Ejercicio2.Ejercicio2Activity;
import com.example.javi.relacionficherosacdat.Ejercicio3.Ejercicio3Activity;
import com.example.javi.relacionficherosacdat.Ejercicio4.Ejercicio4Activity;
import com.example.javi.relacionficherosacdat.Ejercicio5.Ejercicio5Activity;
import com.example.javi.relacionficherosacdat.Ejercicio6.Ejercicio6Activity;
import com.example.javi.relacionficherosacdat.Ejercicio7.Ejercicio7Activity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private Button btnEj1, btnEj2, btnEj3, btnEj4, btnEj5, btnEj6, btnEj7;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnEj1 = (Button)findViewById(R.id.button);
        btnEj1.setOnClickListener(this);
        btnEj2 = (Button)findViewById(R.id.button2);
        btnEj2.setOnClickListener(this);
        btnEj3 = (Button)findViewById(R.id.button3);
        btnEj3.setOnClickListener(this);
        btnEj4 = (Button)findViewById(R.id.button4);
        btnEj4.setOnClickListener(this);
        btnEj5 = (Button)findViewById(R.id.button5);
        btnEj5.setOnClickListener(this);
        btnEj6 = (Button)findViewById(R.id.button6);
        btnEj6.setOnClickListener(this);
        btnEj7 = (Button)findViewById(R.id.button7);
        btnEj7.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()){
            case R.id.button:
                intent = new Intent(MainActivity.this, Ejercicio1Activity.class);
                startActivity(intent);
                break;
            case R.id.button2:
                intent = new Intent(MainActivity.this, Ejercicio2Activity.class);
                startActivity(intent);
                break;
            case R.id.button3:
                intent = new Intent(MainActivity.this, Ejercicio3Activity.class);
                startActivity(intent);
                break;
            case R.id.button4:
                intent = new Intent(MainActivity.this, Ejercicio4Activity.class);
                startActivity(intent);
                break;
            case R.id.button5:
                intent = new Intent(MainActivity.this, Ejercicio5Activity.class);
                startActivity(intent);
                break;
            case R.id.button6:
                intent = new Intent(MainActivity.this, Ejercicio6Activity.class);
                startActivity(intent);
                break;
            case R.id.button7:
                intent = new Intent(MainActivity.this, Ejercicio7Activity.class);
                startActivity(intent);
                break;
        }
    }
}
