package com.example.javi.relacionficherosacdat.Ejercicio1;

import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.example.javi.relacionficherosacdat.R;

public class AddContactoActivity extends AppCompatActivity {

    private EditText etNombre, etTelefono, etMail;
    private FloatingActionButton fabGuardarContacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_contacto);
        etNombre = (EditText)findViewById(R.id.etAddNombre);
        etTelefono = (EditText)findViewById(R.id.etAddTelefono);
        etMail = (EditText)findViewById(R.id.etAddMail);
        fabGuardarContacto = (FloatingActionButton)findViewById(R.id.fabGuardarContacto);
        fabGuardarContacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if ((etNombre.getText().toString() != "") && (etTelefono.getText().toString() != "") && (etMail.getText().toString() != "")){
                    Contacto contacto = new Contacto(etNombre.getText().toString(), etTelefono.getText().toString(), etMail.getText().toString());
                    if ( Agenda.addContacto(contacto)) {
                        Snackbar.make(findViewById(R.id.clAnadirContactos),"Contacto añadido con exito!" , Snackbar.LENGTH_LONG).show();
                    }else{
                        Snackbar.make(findViewById(R.id.clAnadirContactos),"Error al añadir el contacto" , Snackbar.LENGTH_LONG).show();
                    }
                }
            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        Agenda.WriteAgendaFileInternal(this);
    }
}
