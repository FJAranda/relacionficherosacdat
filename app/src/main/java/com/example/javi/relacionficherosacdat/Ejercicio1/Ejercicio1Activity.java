package com.example.javi.relacionficherosacdat.Ejercicio1;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.example.javi.relacionficherosacdat.R;

import java.util.List;

public class Ejercicio1Activity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private rvAgendaAdapter adapter;
    private List<Contacto> agenda;
    private FloatingActionButton fabAnadirContacto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ejercicio1);
        Agenda.ReadAgendaFileInternal(this);
        agenda = Agenda.getAgenda();
        recyclerView = (RecyclerView)findViewById(R.id.rvContactos);
        LinearLayoutManager llmContactos = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llmContactos);
        adapter = new rvAgendaAdapter(agenda);
        recyclerView.setAdapter(adapter);

        fabAnadirContacto = (FloatingActionButton)findViewById(R.id.fabAnadirContacto);
        fabAnadirContacto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Ejercicio1Activity.this, AddContactoActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        agenda.clear();
        Agenda.ReadAgendaFileInternal(this);
        adapter.notifyDataSetChanged();
    }
}
